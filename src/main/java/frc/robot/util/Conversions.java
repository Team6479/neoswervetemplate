package frc.robot.util;

public class Conversions {

    /**
     * @param positionCounts CANCoder Position Counts
     * @param gearRatio Gear Ratio between CANCoder and Mechanism
     * @return Degrees of Rotation of Mechanism
     */
    public static double CANcoderToDegrees(double positionCounts, double gearRatio) {
        return positionCounts * (360.0 / (gearRatio * 4096.0));
    }
    /**
     * @param degrees Degrees of rotation of Mechanism
     * @param gearRatio Gear Ratio between CANCoder and Mechanism
     * @return CANCoder Position Counts
     */
    public static double degreesToCANcoder(double degrees, double gearRatio) {
        return degrees / (360.0 / (gearRatio * 4096.0));
    }

    /**
     * @param counts Neo Position Counts
     * @param gearRatio Gear Ratio between Neo and Mechanism
     * @return Degrees of Rotation of Mechanism
     */
    public static double neoToDegrees(double positionCounts, double gearRatio) {
        return positionCounts * (360.0 / (gearRatio * 1.0));
    }

    /**
     * @param degrees Degrees of rotation of Mechanism
     * @param gearRatio Gear Ratio between Neo and Mechanism
     * @return Neo Position Counts
     */
    public static double degreesToNeo(double degrees, double gearRatio) {
        return degrees / (360.0 / (gearRatio * 1.0));
    }

    /**
     * @param velocityCounts Neo Motor RPM
     * @param gearRatio Gear Ratio between Neo and Mechanism (set to 1 for Neo RPM)
     * @return RPM of Mechanism
     */
    public static double neoToRPM(double motorRPM, double gearRatio) {
        double mechRPM = motorRPM / gearRatio;
        return mechRPM;
    }

    /**
     * @param RPM RPM of mechanism
     * @param gearRatio Gear Ratio between Neo and Mechanism (set to 1 for Neo RPM)
     * @return RPM of Mechanism
     */
    public static double RPMToNeo(double RPM, double gearRatio) {
        double motorRPM = RPM * gearRatio;
        return motorRPM;
    }

    /**
     * @param motorRPM Neo Motor RPM
     * @param circumference Circumference of Wheel
     * @param gearRatio Gear Ratio between Neo and Mechanism (set to 1 for Neo MPS)
     * @return Velocity MPS
     */
    public static double neoToMPS(double motorRPM, double circumference, double gearRatio){
        double wheelRPM = neoToRPM(motorRPM, gearRatio);
        double wheelMPS = (wheelRPM * circumference) / 60;
        return wheelMPS;
    }

    /**
     * @param velocity Velocity MPS
     * @param circumference Circumference of Wheel
     * @param gearRatio Gear Ratio between Neo and Mechanism (set to 1 for Neo MPS)
     * @return Neo Motor RPM
     */
    public static double MPSToNeo(double velocity, double circumference, double gearRatio){
        double wheelRPM = ((velocity * 60) / circumference);
        double wheelVelocity = RPMToNeo(wheelRPM, gearRatio);
        return wheelVelocity;
    }

    /**
     * @param positionCounts Neo Position Counts
     * @param circumference Circumference of Wheel
     * @param gearRatio Gear Ratio between Neo and Wheel
     * @return Meters
     */
    public static double neoToMeters(double positionCounts, double circumference, double gearRatio){
        return positionCounts * (circumference / (gearRatio * 1.0));
    }

    /**
     * @param meters Meters
     * @param circumference Circumference of Wheel
     * @param gearRatio Gear Ratio between Neo and Wheel
     * @return Neo Position Counts
     */
    public static double metersToNeo(double meters, double circumference, double gearRatio){
        return meters / (circumference / (gearRatio * 1.0));
    }
}